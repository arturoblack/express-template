const db = require('../models');
const Usuario = db.Usuario;

exports.list = async function(req, res) {
  try {
    const usuarios = Usuario.findAll();
    res.json(usuarios);
  } catch(err) {
    res.status(500).json({error: err});
  }
}

exports.get = async function(req, res) {
  try {
    const id = req.params.id;
    const usuario = Usuario.findOne({where: {id: id}});
    res.json(usuario);
  } catch(err) {
    res.status(500).json({error: err});
  }
}

// POST /productos/
exports.create = async function(req, res) {
  try{
      const nuevoProd = await Producto
                                 .create(req.body);
      res.json(nuevoProd);
  } catch(err){
      res.status(404).json({error: err});
  }
};

// PUT /productos/5
exports.update = async function(req, res) {
  const id = req.params.id;
  try {
      await Producto.update(req.body,{where: {id: id}});
      res.json({ok: 'ok'})
        
  } catch(error){}
};

//DELETE //productos/9
exports.remove = async function(req, res) {
  const id = req.params.id;
  try {
      await Producto.destroy({where: {id: id} });
      res.json({ok: 'ok'})
        
  } catch(error){
      res.status(404).json({error: err});
  }
};

//tareas por usuario

exports.tareas = async function(req, res) {
  try {
    const id = req.params.id;
    const usuario = Usuario.findOne({
      where: {id: id}, 
      include: [{
        model: db.Tarea,
        attributes: ['titulo', 'fecha']
      }]
    });
    res.json(usuario);
  } catch(err) {
    res.status(500).json({error: err});
  }
}