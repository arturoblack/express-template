const express = require('express');
const router = express.Router();
const usuarioCtrl = require('../controllers/usuarios.controller');

/* GET home page. */
router.get('/', usuarioCtrl.list);
router.get('/:id', usuarioCtrl.get);
router.get('/:id/tareas', usuarioCtrl.tareas);
router.post('/:id', usuarioCtrl.create);
router.put('/:id', usuarioCtrl.update);
router.delete('/:id', usuarioCtrl.remove);

module.exports = router;
